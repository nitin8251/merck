import json
import boto3
import time
import sys
from awsglue.utils import getResolvedOptions
def query_athena(query,database,outputlocation):
    client=boto3.client('athena')
    queryStart=client.start_query_execution(
            QueryString= query,
            QueryExecutionContext={
                'Database' : database
            },
            ResultConfiguration = {
                'OutputLocation': outputlocation
            }
        )
    queryId = queryStart['QueryExecutionId']
    time.sleep(15)

    results = client.get_query_results(QueryExecutionId=queryId)
    for row in results['ResultSet']['Rows']:
        print(row)
    return results    
def main():
    args = getResolvedOptions(sys.argv, ['query','database','outputlocation'])
    query=args ['query']
    database= args['database']
    outputlocation= args['outputlocation']
    print(query)
    print(database)
    print(outputlocation)
    result=query_athena(query,database,outputlocation)
if __name__ == "__main__":
    main()