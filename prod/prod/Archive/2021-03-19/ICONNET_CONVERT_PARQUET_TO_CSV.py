import sys
import boto3
import datetime
import json
import linecache
import pyspark
from pyspark.sql import SparkSession
import pyspark.sql.functions as f
from collections import OrderedDict
from pyspark.sql.functions import udf, col, split, when, array, count
from pyspark.sql.types import ArrayType, IntegerType,StringType, NullType, StructType, StructField


from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue.utils import getResolvedOptions

def convert_parquet_to_csv(spark, source_file_path,target_file_path,no_partitions):
    try:
        tmp_df=spark.read.parquet(source_file_path)
        tmp_df.coalesce(no_partitions).write.format('csv').mode('overwrite').save(target_file_path)
        return None
    except:
        return "Error ! File did not converted"
def main():
    # Passing Arguements
    args = getResolvedOptions(sys.argv, ['source_file_path','target_file_path','no_partitions'])
    start_time = datetime.datetime.now()
    source_file_path = args ['source_file_path']
    target_file_path = args ['target_file_path']
    no_partitions = int(args ['no_partitions'])
    
    
    sc = pyspark.SparkContext()
    spark=SparkSession(sc)
    glueContext=GlueContext(sc)
    
    print("Parquet_CSV Conversion - Started")
    
    e = convert_parquet_to_csv (spark, source_file_path,target_file_path,no_partitions)
    print("Parquet_CSV Conversion - Completed")
    
    return
if __name__ == "__main__":
    main()