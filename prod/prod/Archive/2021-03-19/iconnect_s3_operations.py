#------------------------------------------------------------------------------------------------------------------------------------------
# Purpose			: s3 operations with functions.
# Environment		: MERCK-DEV-ICONNET
# Created by 		: INFOSYS
# Created date 		: 2021-02-16
# Description 		: The Complete code available S3 Operations - delete | copy files etc 
#------------------------------------------------------------------------------------------------------------------------------------------
# Modification History
#------------------------------------------------------------------------------------------------------------------------------------------
# Modified By 		: NA
# Modified Date 	: NA
# Description 		: NA
#------------------------------------------------------------------------------------------------------------------------------------------
# Modified By 		: NA
# Modified Date 	: NA
# Description 		: NA
#------------------------------------------------------------------------------------------------------------------------------------------
import sys
import boto3
import datetime
import json
import linecache
import pyspark
#import module


from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue.utils import getResolvedOptions
from pyspark.sql.functions import col, concat_ws, collect_list, explode, lit, split, when, upper

class S3_OPERATIONS_ERROR (Exception):
    def __init__(self, m):
        self.message = m
    def __str__(self):
        return self.message
class gbl_objects:
    objectname = None
    s3_bucket = None
    s3_data_current = None
    s3_data_timestamp = None
    s3_result_current = None
    s3_result_timestamp = None
    s3_intermediate_path = None
    s3_tmp_path = None
    object_flags = None
    result_name = None

def sfdc_exception(Debug=True):
    exc_type, exc_obj,tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    file_name = f.f_code.co_filename
    linecache.checkcache(file_name)
    line = linecache.getline(file_name, lineno, f.f_globals)
    error_obj = {'error_code':str(exc_type),'error_message':str(exc_obj)}
    #print 'ERROR: EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj)
    error_obj['error_at']="ERROR: EXCEPTION IN ('{}', LINE '{}' '{}')".format(file_name, lineno, line.strip())
    format_error_msg = "ERROR CODE : {}; REASON :{}; LINE_AT : {} ".format(error_obj['error_code'],error_obj['error_message'],error_obj['error_at'])
    raise S3_OPERATIONS_ERROR (format_error_msg)
    return error_obj

def s3_result_delete_current (logger,bucket):
    try:
    	print ("Deletion....s3_result_delete_current")
    	s3_current_path = s3_path_to_current(logger,gbl_objects.s3_data_current)
    	delete_s3_folder (logger,bucket, s3_current_path)
    	return
    except:
        e=sfdc_exception()
        logger.error(e)
    return None
def attribute_value_extract(key):
    try:
        value=key
        object_flags = gbl_objects.object_flags
        
        #selective_field_text = object_flags[gbl_objects.objectname]["columns"].lower()
        for attribute in object_flags[gbl_objects.objectname]:
            if key == attribute:
               value = object_flags[gbl_objects.objectname][key]
               break
        return value
    except:
        e=sfdc_exception()
        logger.error(e)
        raise e
def s3_file_exists (logger,bucket,s3_path):
    try:
        s3_path = s3_path.split(bucket+"/")[1]
        
        client = boto3.client('s3')
        results = client.list_objects(Bucket=bucket, Prefix=s3_path)
        return 'Contents' in results
    except:
        e=sfdc_exception()
        logger.error(e)
        return None
def s3_data_update_reload_current_v2(logger,bucket):
    try:
        print ("Deletion.... s3_data_update_reload_current_v2")
        
        #Delete current/data
        
        
        s3_current_path = gbl_objects.s3_result_current
        s3_current_path = s3_current_path.replace ("{result_name}",gbl_objects.result_name)
        s3_current_path = s3_current_path.replace ("{load_type}","full")
        s3_current_path = s3_path_to_current(logger,s3_current_path) 
        delete_s3_folder (logger,bucket, s3_current_path)
        #copy from tmp to current/data
        
        s3_tmp_path = s3_path_to_current(logger,gbl_objects.s3_tmp_path) + gbl_objects.result_name+"/full/" 
        
        if s3_file_exists(logger,bucket,s3_tmp_path):
            copy_s3_files(logger,bucket,s3_tmp_path,s3_current_path)
            print ("Copying.... '{}' to '{}'".format(s3_tmp_path,s3_current_path))
            delete_s3_folder (logger,bucket, s3_tmp_path)
            s3_data_update_copy_current_to_ts (logger,bucket,s3_current_path,"full")
        else:
            print ("No files under.... '{}' for copying to '{}'".format(s3_tmp_path,s3_current_path))
        
        s3_current_path = gbl_objects.s3_result_current
        s3_current_path = s3_current_path.replace ("{result_name}",gbl_objects.result_name)
        s3_current_path = s3_current_path.replace ("{load_type}","delta")
        s3_current_path = s3_path_to_current(logger,s3_current_path)
        delete_s3_folder (logger,bucket, s3_current_path)
        #copy from tmp to current/data
        
        s3_tmp_path = s3_path_to_current(logger,gbl_objects.s3_tmp_path) + gbl_objects.result_name+"/delta/" 
        copy_s3_files(logger,bucket,s3_tmp_path,s3_current_path)
        print ("Copying.... '{}' to '{}'".format(s3_tmp_path,s3_current_path))
        delete_s3_folder (logger,bucket, s3_tmp_path)
        s3_data_update_copy_current_to_ts (logger,bucket,s3_current_path,"delta")
        return
    except:
        e=sfdc_exception()
        logger.error(e)
    return None
def s3_data_update_copy_current_to_ts (logger,bucket,s3_current_path,load_type):
    try:
        print ("Copying.... s3_data_update_copy_current_to_ts")
        #s3_current_path = s3_current_path
        s3_result_timestamp = gbl_objects.s3_result_timestamp
        s3_result_timestamp = s3_result_timestamp.replace ("{result_name}",gbl_objects.result_name)
        s3_result_timestamp = s3_result_timestamp.replace("{load_type}", load_type)
        s3_timestamp_path = s3_path_to_timestamp(logger,s3_result_timestamp)
        copy_s3_files(logger,bucket,s3_current_path,s3_timestamp_path)
        return
    except:
        e=sfdc_exception()
        logger.error(e)
    return None
def s3_raw_reload_current_v2(logger,bucket):
    try:
        print ("Deletion.... s3_raw_reload_current_v2")
        
        #Delete current/data
        s3_current_path = s3_path_to_current(logger,gbl_objects.s3_data_current)
        delete_s3_folder (logger,bucket, s3_current_path)
        #copy from tmp to current/data
        if  attribute_value_extract("filter_key").strip() == "":
            s3_tmp_path = s3_path_to_current(logger,gbl_objects.s3_tmp_path) +  gbl_objects.objectname + "/"
            copy_s3_files(logger,bucket,s3_tmp_path,s3_current_path)
            print ("Copying.... '{}' to '{}'".format(s3_tmp_path,s3_current_path))
            delete_s3_folder (logger,bucket, s3_tmp_path)
        else:
            for filter_value in attribute_value_extract("filter_key").split(","):
                s3_tmp_path = s3_path_to_current(logger,gbl_objects.s3_tmp_path) +  gbl_objects.objectname + "/" + filter_value.strip() + "/"
                copy_s3_files(logger,bucket,s3_tmp_path,s3_current_path)
                print ("Copying.... '{}' to '{}'".format(s3_tmp_path,s3_current_path))
                delete_s3_folder (logger,bucket, s3_tmp_path)
        s3_raw_copy_current_to_ts (logger,bucket)
        return
    except:
        e=sfdc_exception()
        logger.error(e)
    return None
def s3_raw_copy_current_to_ts (logger,bucket):
    try:
        if attribute_value_extract("history_required").lower() == "yes":
        	print ("Copying.... s3_raw_copy_current_to_ts")
        	s3_current_path = s3_path_to_current(logger,gbl_objects.s3_data_current)
        	s3_timestamp_path = s3_path_to_timestamp(logger,gbl_objects.s3_data_timestamp)
        	copy_s3_files(logger,bucket,s3_current_path,s3_timestamp_path)
        else:
            print ("History is not required.... s3_raw_copy_current_to_ts")
        return
    except:
        e=sfdc_exception()
        logger.error(e)
    return None
def s3_result_delete_current (logger,bucket):
    try:
    	print ("Deletion....s3_result_delete_current")
    	s3_current_path = s3_path_to_current(logger,gbl_objects.s3_result_current)
    	delete_s3_folder (logger,bucket, s3_current_path)
    	return
    except:
        e=sfdc_exception()
        logger.error(e)
    return None
def s3_result_copy_current_to_ts (logger,bucket):
    try:
        """
        if attribute_value_extract("history_required").lower() == "yes":
        	print ("Copying....s3_result_copy_current_to_ts")
        	s3_current_path = s3_path_to_current(logger,gbl_objects.s3_result_current)
        	s3_timestamp_path = s3_path_to_timestamp(logger,gbl_objects.s3_result_timestamp)
        	copy_s3_files(logger,bucket,s3_current_path,s3_timestamp_path)
        else:
            print ("History is not required.... s3_result_copy_current_to_ts")
        return
        """
        print ("Copying....s3_result_copy_current_to_ts")
        s3_current_path = s3_path_to_current(logger,gbl_objects.s3_result_current)
        s3_timestamp_path = s3_path_to_timestamp(logger,gbl_objects.s3_result_timestamp)
        copy_s3_files(logger,bucket,s3_current_path,s3_timestamp_path)
        return
    except:
        e=sfdc_exception()
        logger.error(e)
        return e
    return None
def s3_list_files_v2 (logger,bucket,source_file_path):
    try:
        bucket_name = bucket
        prefix  = source_file_path.split(bucket+"/")[1]
    
        s3_conn = boto3.client('s3')  # type: BaseClient  ## again assumes boto.cfg setup, assume AWS S3
        s3_result = s3_conn.list_objects_v2(Bucket=bucket_name, Prefix=prefix, Delimiter = "/")
    
        if 'Contents' not in s3_result:
            #print(s3_result)
            return []
    
        file_list = []
        for key in s3_result['Contents']:
            file_list.append(key['Key'])
        print("List count = {}".format(len(file_list)))
        while s3_result['IsTruncated']:
            continuation_key = s3_result['NextContinuationToken']
            s3_result = s3_conn.list_objects_v2(Bucket=bucket_name, Prefix=prefix, Delimiter="/", ContinuationToken=continuation_key)
            for key in s3_result['Contents']:
                file_list.append(key['Key'])
            print("List count = {}".format(len(file_list)))
        
        return file_list
    except:
        e=sfdc_exception()
        logger.error(e)
        
    return None
def copy_s3_files(logger,bucket,source_file_path,target_file_path):
    try:
        target_file_path = target_file_path.split(bucket+"/")[1]
        file_list = s3_list_files_v2 (logger,bucket,source_file_path)
        if len(file_list)==0:
            print("No Files to copy from {} to {}".format(source_file_path,target_file_path))
            logger.info("No Files to copy from {} to {}".format(source_file_path,target_file_path))
            return False
        else:
            s3 = boto3.resource('s3')
            for file_names in file_list:
                copy_source = {
                      'Bucket': bucket,
                      'Key': file_names
                    }
                file_name=str(file_names).split("/")
                #bucket.copy(copy_source, 'data/output/outputfile')
                s3.meta.client.copy(copy_source, bucket, target_file_path + file_name[len(file_name)-1])
            print("Files copied successfully from {} to {}".format(source_file_path,target_file_path))
            logger.info("Files copied successfully from {} to {}".format(source_file_path,target_file_path))
            return False
    except:
        e=sfdc_exception()
        logger.error(e)
        
    return None

def delete_s3_folder (logger,bucket, s3_current_path):
    try:
        
        bucket_name  = bucket
        prefix = s3_current_path.split(bucket+"/")[1]
        s3 = boto3.resource('s3')
        bucket = s3.Bucket(bucket_name)
        print ("Prefix " + prefix)
        bucket.objects.filter(Prefix=prefix).delete()
        print ("SFDC - S3 path '{}' deleted successfully ".format(s3_current_path))
        logger.info("SFDC - S3 path '{}' deleted successfully ".format(s3_current_path))
        return False
    except:
        e=sfdc_exception()
        logger.error(e)
        
    return None
    

def delete_s3_files (bucket, control_file_path):
    try:
        bucket_name  = bucket
        prefix = control_file_path
        
        s3_client = boto3.client('s3')
        response = s3_client.list_objects_v2(Bucket=bucket, Prefix=prefix)
        
        if 'Contents' not in response:
            print ("No Files found in path")
            return 
        for object in response['Contents']:
            if object['Key'] != prefix:
                print('Deleting', object['Key'])
                s3_client.delete_object(Bucket=bucket, Key=object['Key'])
        print ("Files Deleted Successfully")
        return
    except:
        e=sfdc_exception()
        logger.error(e)
    return None
def read_control_file (logger,bucket,key):
    try:
    
        s3obj = boto3.client('s3') 
        #bucket = 'iconnectuser' 
        #key = 'scripts/move_file/app/control.json'
        s3_file = s3obj.get_object(Bucket = bucket , Key = key) 
        params = s3_file['Body'].read().decode('utf-8')
        control_file = json.loads(params)
    
        gbl_objects.s3_bucket = control_file["s3_bucket"]
        gbl_objects.object_flags = control_file["object_flags"]
        gbl_objects.s3_data_current = control_file["s3_data_current"]
        gbl_objects.s3_data_timestamp  = control_file["s3_data_timestamp"]
        gbl_objects.s3_result_current = control_file["s3_result_current"]
        gbl_objects.s3_result_timestamp = control_file["s3_result_timestamp"]
        gbl_objects.s3_intermediate_path = control_file["s3_intermediate_path"]
        gbl_objects.s3_tmp_path = control_file["s3_tmp_path"]
        gbl_objects.result_name = control_file["result_name"]
        
        logger.info("SFDC - read_control_file successfully completed ")
        return False
    except:
        e=sfdc_exception()
        logger.error(e)
        return e
def s3_path_to_current(logger,s3_path):
    try:
        s3_path = s3_path.replace("{s3_bucket}",gbl_objects.s3_bucket)
        s3_path = s3_path.replace("{objectname}",gbl_objects.objectname)
        #gbl_objects.s3_data_current = s3_path 
        
        logger.info("SFDC - Current S3 path is '{}' ".format(s3_path))
        return s3_path
    except:
        e=sfdc_exception()
        logger.error(e)
    return None
def s3_path_to_timestamp(logger,s3_path):
    try:
        now = datetime.datetime.today() 
        timestamp_path = now.strftime("%Y-%m-%d/%H-%M-%S")
        s3_path = s3_path.replace("{s3_bucket}",gbl_objects.s3_bucket)
        s3_path = s3_path.replace("{objectname}",gbl_objects.objectname)
        s3_path = s3_path.replace("{timestamp}",timestamp_path)
        #gbl_objects.s3_data_timestamp = s3_path 
        logger.info("SFDC - Timestamp S3 path is '{}' ".format(s3_path))
        return s3_path
    except:
        e=sfdc_exception()
        logger.error(e)
    return None
def main():
    args = getResolvedOptions(sys.argv, ['bucket','control_file_path','objectname','operation'])
    #start_time = datetime.datetime.now()
    bucket = args ['bucket']
    key = args ['control_file_path']
    objectname = args ['objectname']
    operation = args ['operation']
    
    sc = pyspark.SparkContext()
    glueContext = GlueContext(sc)
    
    logger = glueContext.get_logger()
    logger.info("SFDC - S3 operation '{}' started on object {} ".format(operation,objectname))
    #logger.warn("warn message")
    #logger.error("error message")
    #control_file_path = 'scripts/move_file/app/control.json'
    gbl_objects.objectname = objectname
    gbl_objects.operation = operation
    #Reading control file
    e = read_control_file (logger,bucket,key)
    if e:
        logger.error(e)
        raise e
    
    operation_call = globals()[operation.lower()]
    operation_call(logger,bucket)
    
    
    print("SFDC - S3 operation '{}' completed on object '{}' ".format(operation,objectname))
    logger.info("SFDC - S3 operation '{}' completed on object '{}' ".format(operation,objectname))
    return
if __name__ == "__main__":
    main()