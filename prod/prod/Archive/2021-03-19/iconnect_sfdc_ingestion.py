#------------------------------------------------------------------------------------------------------------------------------------------
# Purpose			: FRAMEWORK of SFDC INGESTION FROM SOURCE TO S3
# Environment		: MERCK-DEV-ICONNET
# Created by 		: INFOSYS
# Created date 		: 2021-02-17
# Description 		: The Complete code available SFDC INGESTION FROM SOURCE TO S3 
#------------------------------------------------------------------------------------------------------------------------------------------
# Modification History
#------------------------------------------------------------------------------------------------------------------------------------------
# Modified By 		: NA
# Modified Date 	: NA
# Description 		: NA
#------------------------------------------------------------------------------------------------------------------------------------------
# Modified By 		: NA
# Modified Date 	: NA
# Description 		: NA
#------------------------------------------------------------------------------------------------------------------------------------------
import sys
import datetime
import json
import requests
import linecache
import base64

import logging
import boto3
import pyspark
from pyspark.sql import Row
from pyspark.sql import SparkSession
from pyspark.sql.column import Column, _to_java_column
from pyspark.sql.types import array, ArrayType, IntegerType, NullType, StringType, StructType, StructField
from pyspark.sql.functions import col, concat_ws, collect_list, explode, lit, split, when, upper

###################GLUE import##############
from awsglue.context import GlueContext
from awsglue.dynamicframe import DynamicFrame
from awsglue.transforms import Relationalize
from awsglue.utils import getResolvedOptions
from awsglue.job import Job
from awsglue.transforms import *

#Account = ID, TERRITORY_VOD__C
#Address_vod__c = ACCOUNT,BRICK_VOD__C,ID,INCLUDE_IN_TERRITORY_ASSIGNMENT_VOD__C
#Brick_to_Terr_vod__c = Name, TERRITORY_VOD__C

#Account = ID,ISEXCLUDEDFROMREALIGN,MCK_PRIMARY_COUNTRY__C
#Address_vod__c =ACCOUNT, BRICK_VOD__C, COUNTRY_VOD__C, ID,INCLUDE_IN_TERRITORY_ASSIGNMENT_VOD__C,CK_COUNTRY__C,ZIP_VOD__C
#Brick_to_Terr_vod__c =ACTIVATEDDATE,DEACTIVATEDDATE,DEVELOPERNAME, ID, NAME,STATE

class SFDC_INGESTION_ERROR (Exception):
    def __init__(self, m):
        self.message = m
    def __str__(self):
        return self.message
        
class gbl_objects:
    
    s3_bucket = None
    baseurl = None
    user_id = None
    password = None
    client_id = None
    client_secret = None
    grant_type = None
    url_for_token = None
    url_for_metadata = None
    url_for_data = None
    
    lazywriter_threshold=None
    s3_data_current=None
    s3_data_timestamp=None
    s3_tmp_path = None
    write_file_format = None
    
    object_attributes = None # data
    object_flags = None # data
    objectname = None
    filter_key = None
    filter_clause = None
    #filter_condition_2 = None
    
    fieldname = "name"
    fieldsoaptype = "soapType"
    field_length = "length"
    field_precision ="precision"
    field_scale = "scale"
    
    objectfieldname= 0
    fielddatatype = 1
    
def sfdc_encrypt (password):
    try:
        password = password.encode('utf-8')
        encoded_text = base64.b64encode(password)
        return encoded_text.decode('utf-8')
    except:
        e = sfdc_exception()
        return e


def sfdc_decrypt (password):
    try:
        password = password.encode('utf-8')
        decoded_text = base64.b64decode(password)
        return decoded_text.decode('utf-8')
    except:
        e = sfdc_exception()
        return e
def sfdc_exception(Debug=True):
    exc_type, exc_obj,tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    file_name = f.f_code.co_filename
    linecache.checkcache(file_name)
    line = linecache.getline(file_name, lineno, f.f_globals)
    error_obj = {'error_code':str(exc_type),'error_message':str(exc_obj)}
    #print 'ERROR: EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj)
    error_obj['error_at']="ERROR: EXCEPTION IN ('{}', LINE '{}' '{}')".format(file_name, lineno, line.strip())
    format_error_msg = "ERROR CODE : {}; REASON :{}; LINE_AT : {} ".format(error_obj['error_code'],error_obj['error_message'],error_obj['error_at'])
    raise SFDC_INGESTION_ERROR (format_error_msg)
    return error_obj

def s3_list_files_v2 (logger,bucket,source_file_path):
    try:
        bucket_name = bucket
        prefix  = source_file_path.split(bucket+"/")[1]
    
        s3_conn = boto3.client('s3')  # type: BaseClient  ## again assumes boto.cfg setup, assume AWS S3
        s3_result = s3_conn.list_objects_v2(Bucket=bucket_name, Prefix=prefix, Delimiter = "/")
    
        if 'Contents' not in s3_result:
            #print(s3_result)
            return []
    
        file_list = []
        for key in s3_result['Contents']:
            file_list.append(key['Key'])
        print("List count = {}".format(len(file_list)))
        while s3_result['IsTruncated']:
            continuation_key = s3_result['NextContinuationToken']
            s3_result = s3_conn.list_objects_v2(Bucket=bucket_name, Prefix=prefix, Delimiter="/", ContinuationToken=continuation_key)
            for key in s3_result['Contents']:
                file_list.append(key['Key'])
            print("List count = {}".format(len(file_list)))
        
        return file_list
    except:
        e=sfdc_exception()
        return e
def copy_s3_files(logger,bucket,source_file_path,target_file_path):
    try:
        target_file_path = target_file_path.split(bucket+"/")[1]
        file_list = s3_list_files_v2 (logger,bucket,source_file_path)
        if len(file_list)==0:
            print("No Files to copy from {} to {}".format(source_file_path,target_file_path))
            logger.info("No Files to copy from {} to {}".format(source_file_path,target_file_path))
            return False
        else:
            s3 = boto3.resource('s3')
            for file_names in file_list:
                copy_source = {
                      'Bucket': bucket,
                      'Key': file_names
                    }
                file_name=str(file_names).split("/")
                #bucket.copy(copy_source, 'data/output/outputfile')
                s3.meta.client.copy(copy_source, bucket, target_file_path + file_name[len(file_name)-1])
            print("Files copied successfully from {} to {}".format(source_file_path,target_file_path))
            logger.info("Files copied successfully from {} to {}".format(source_file_path,target_file_path))
            return False
    except:
        e=sfdc_exception()
        return e

def read_control_file (bucket,key):
    try:
    
        s3obj = boto3.client('s3') 
        #bucket = 'iconnectuser' 
        #key = 'scripts/move_file/app/control.json'
        s3_file = s3obj.get_object(Bucket = bucket , Key = key) 
        params = s3_file['Body'].read().decode('utf-8')
        control_file = json.loads(params)
    
        gbl_objects.s3_bucket = control_file["s3_bucket"]
        gbl_objects.baseurl = control_file["baseurl"]
        gbl_objects.user_id = control_file["user_id"]
        gbl_objects.password = sfdc_decrypt(control_file["password"]) #sfdc_decrypt(enc_password)
        gbl_objects.client_id = control_file["client_id"]
        gbl_objects.client_secret = control_file["client_secret"]
        gbl_objects.grant_type = control_file["grant_type"]
        gbl_objects.url_for_token = control_file["url_for_token"]
        gbl_objects.url_for_metadata = control_file["url_for_metadata"]
        gbl_objects.url_for_data = control_file["url_for_data"]
        
        gbl_objects.lazywriter_threshold = control_file["lazywriter_threshold"]
        gbl_objects.s3_data_current = control_file["s3_data_current"]
        gbl_objects.s3_data_timestamp = control_file["s3_data_timestamp"]
        gbl_objects.s3_tmp_path = control_file["s3_tmp_path"]
        
        gbl_objects.s3_data_current = gbl_objects.s3_data_current.replace('{s3_bucket}',gbl_objects.s3_bucket)
        gbl_objects.s3_data_timestamp = gbl_objects.s3_data_timestamp.replace('{s3_bucket}',gbl_objects.s3_bucket)
        gbl_objects.s3_tmp_path = gbl_objects.s3_tmp_path.replace('{s3_bucket}',gbl_objects.s3_bucket)
        
        #gbl_objects.object_attributes = control_file["object_attributes"]
        gbl_objects.object_flags = control_file["object_flags"]
        gbl_objects.write_file_format = control_file["write_file_format"]
        
        #filter_str = gbl_objects.filter_key.split("filter_condition_"+,1)[1] 
        #filter_str = "filter_condition_"+filter_str
        
        #if control_file.has_key(gbl_objects.filter_key):
        if gbl_objects.filter_key in control_file:
            gbl_objects.filter_clause = control_file[gbl_objects.filter_key]
        else:
            gbl_objects.filter_clause = None
        """
        if gbl_objects.filter_key == "filter_condition_2":
            gbl_objects.filter_clause = control_file["filter_condition_2"]
        """
        
        
        return False
    except:
        e=sfdc_exception()
        return e
def attribute_value_extract(key):
    try:
        #key = "active_flag"
        value=key
        object_flags = gbl_objects.object_flags
        
        #selective_field_text = object_flags[gbl_objects.objectname]["columns"].lower()
        for attribute in object_flags[gbl_objects.objectname]:
            if key == attribute:
               value=object_flags[gbl_objects.objectname][key]
               break;
        return value
    except:
        e = sfdc_exception()
        return e
def sfdc_process(spark,logger):
    
    try:
        if attribute_value_extract("active_flag").lower() == "yes":
            print("INFO: SFDC ingestion process started")
            l_field_names, l_field_metadata, schema, e = sfdc_getmetadata_v2(logger)
            if e:
                print (e)
                return
            requesturl = sfdc_getsoqlqueryurl(logger,l_field_names,schema)
            
            #s3_clear_current_path()
            e = sfdc_getdata_v2(spark,logger,requesturl,l_field_names,schema,l_field_metadata)
            if e:
                print (e)
                return
        else:
            print("INFO: SFDC ingestion process not required  since active_flag is set as 'No'")
        return
    except:
        e = sfdc_exception()
        return e

def sfdc_getdata_v2(spark,logger,request_url,l_field_names,schema,l_field_metadata):
    try:
        l_rows = []
        counter = 0
        record_count = 0
        lazywriter_threshold = int(gbl_objects.lazywriter_threshold) #100 #gbl_objects.lazywriter_threshold #gbl_objects.lazywriter_threshold #100
        write_mode = 'overwrite'
        write_file_format = gbl_objects.write_file_format
        
        no_partitions = int(attribute_value_extract("no_of_partitions"))
        #if gbl_objects.objectname != 'Brick_to_Terr_vod__c':
        #    no_partitions = 30
        s3_data_current= gbl_objects.s3_data_current #"s3://iconnectuser/data/{objectname}/current/"
        s3_data_timestamp= gbl_objects.s3_data_timestamp #"s3://iconnectuser/data/{objectname}/{timestamp}/"
        s3_tmp_path = gbl_objects.s3_tmp_path
        main_df = spark.createDataFrame(data=[], schema=schema)

        print ("INFO: Debug - Data pull from source started")
        #authkey, e = sfdc_accesstoken()
        sfdc_auth_key, e = sfdc_access_token()
        sfdc_headers = {'Authorization': 'Bearer ' + sfdc_auth_key,
                    'Cache-Control':'no-cache',
                    'charset':'UTF-8'
                    }
        response = requests.get(request_url, headers=sfdc_headers)
        print("INFO: Requested URL '{}' of data for object '{}'".format(request_url,gbl_objects.objectname))
        
        print("INFO: API respone status code '{}' data".format(response.status_code))
        if response.status_code > 300:
            print ("Error status code")
            error_msg = "ERROR: error while pulling data"
            raise AttributeError(error_msg)
        else:
            d_object_data=response.json()
            for row_data in d_object_data.get("records"):
                l_temp_row =[]
                for source_field_name in l_field_names:
                    l_temp_row.append(row_data.get(source_field_name))
                l_rows.append(l_temp_row)
                
            while True:
                #print ("INFO: get done {}".format(str(d_object_data.get("done"))))
                if str(d_object_data.get("done")) == "False":
                    counter = counter + 1
                    if counter != int(lazywriter_threshold): # if counter != int(self.lazywriter_thershold):lazywriter_threshold =100
                        request_url = gbl_objects.baseurl + d_object_data.get('nextRecordsUrl')
                        #print ("INFO: Next set url is {}".format(d_object_data['nextRecordsUrl']))
                        response = requests.get(request_url, headers=sfdc_headers)
                        d_object_data = response.json()
                        for row_data in d_object_data.get("records"):
                            l_temp_row =[]
                            for source_field_name in l_field_names:
                                l_temp_row.append(row_data.get(source_field_name))
                            l_rows.append(l_temp_row)
                        
                    else:
                        print("INFO: Threshold value reached pushing data from memory to S3")
                        record_count = record_count + len(l_rows)
                        df = spark.createDataFrame(l_rows,schema=schema)
                        main_df = main_df.union(df)
                        
                        s3_data_current = s3_data_current.replace('{objectname}',gbl_objects.objectname)
                        #df.coalesce(1).write.format(write_file_format).mode(write_mode).save(s3_data_current)
                        #write_mode = 'append'
                        print("INFO: Records '{}' found in object '{}' as part of nextRecordsUrl".format(len(l_rows),gbl_objects.objectname))
                        counter = -1
                        l_rows = []
                        continue
                else:
                    record_count = record_count + len(l_rows)
                    df = spark.createDataFrame(l_rows,schema=schema)
                    main_df = main_df.union(df)
                    s3_data_current = s3_data_current.replace('{objectname}',gbl_objects.objectname)
                    if gbl_objects.filter_key.strip() == "":
                        s3_tmp_path = s3_tmp_path + gbl_objects.objectname +"/"
                    else:
                        s3_tmp_path = s3_tmp_path + gbl_objects.objectname +"/" + gbl_objects.filter_key
                    
                    main_df.coalesce(no_partitions).write.format(write_file_format).mode(write_mode).save(s3_tmp_path)
                    
                    #copy_s3_files(logger,gbl_objects.s3_bucket,s3_tmp_path,s3_data_current)
                    #write_mode = 'append'
                    print("INFO: Total Records '{}' found in object '{}'".format(record_count,gbl_objects.objectname))
                    print("INFO: Successfully downloaded all records of the object '{}' to S3".format(gbl_objects.objectname))
                    break
        return False
    except AttributeError as e:
        print (e)
    except:
        e=sfdc_exception()
        return None, e
def sfdc_getdata(spark,request_url,l_field_names,schema,l_field_metadata):
    try:
        l_rows = []
        counter = 0
        record_count = 0
        lazywriter_threshold = int(gbl_objects.lazywriter_threshold) #100 #gbl_objects.lazywriter_threshold #gbl_objects.lazywriter_threshold #100
        write_mode = 'append'
        write_file_format = gbl_objects.write_file_format
        no_partitions = 1
        s3_data_current= gbl_objects.s3_data_current #"s3://iconnectuser/data/{objectname}/current/"
        s3_data_timestamp= gbl_objects.s3_data_timestamp #"s3://iconnectuser/data/{objectname}/{timestamp}/"
        print ("INFO: Debug - Data pull from source started")
        #authkey, e = sfdc_accesstoken()
        sfdc_auth_key, e = sfdc_access_token()
        sfdc_headers = {'Authorization': 'Bearer ' + sfdc_auth_key,
                    'Cache-Control':'no-cache',
                    'charset':'UTF-8'
                    }
        response = requests.get(request_url, headers=sfdc_headers)
        print("INFO: Requested URL '{}' of data for object '{}'".format(request_url,gbl_objects.objectname))
        
        print("INFO: API respone status code '{}' data".format(response.status_code))
        if response.status_code > 300:
            print ("Error status code")
            error_msg = "ERROR: error while pulling data"
            raise AttributeError(error_msg)
        else:
            d_object_data=response.json()
            for row_data in d_object_data.get("records"):
                l_temp_row =[]
                for source_field_name in l_field_names:
                    l_temp_row.append(row_data.get(source_field_name))
                l_rows.append(l_temp_row)
                
            while True:
                #print ("INFO: get done {}".format(str(d_object_data.get("done"))))
                if str(d_object_data.get("done")) == "False":
                    counter = counter + 1
                    if counter != int(lazywriter_threshold): # if counter != int(self.lazywriter_thershold):lazywriter_threshold =100
                        request_url = gbl_objects.baseurl + d_object_data.get('nextRecordsUrl')
                        #print ("INFO: Next set url is {}".format(d_object_data['nextRecordsUrl']))
                        response = requests.get(request_url, headers=sfdc_headers)
                        d_object_data = response.json()
                        for row_data in d_object_data.get("records"):
                            l_temp_row =[]
                            for source_field_name in l_field_names:
                                l_temp_row.append(row_data.get(source_field_name))
                            l_rows.append(l_temp_row)
                        
                    else:
                        print("INFO: Threshold value reached pushing data from memory to S3")
                        record_count = record_count + len(l_rows)
                        df = spark.createDataFrame(l_rows,schema=schema)
                        s3_data_current = s3_data_current.replace('{objectname}',gbl_objects.objectname)
                        df.coalesce(1).write.format(write_file_format).mode(write_mode).save(s3_data_current)
                        write_mode = 'append'
                        print("INFO: Records '{}' found in object '{}' as part of nextRecordsUrl".format(len(l_rows),gbl_objects.objectname))
                        counter = -1
                        l_rows = []
                        continue
                else:
                    record_count = record_count + len(l_rows)
                    df = spark.createDataFrame(l_rows,schema=schema)
                    s3_data_current = s3_data_current.replace('{objectname}',gbl_objects.objectname)
                    df.coalesce(1).write.format(write_file_format).mode(write_mode).save(s3_data_current)
                    write_mode = 'append'
                    print("INFO: Total Records '{}' found in object '{}'".format(record_count,gbl_objects.objectname))
                    print("INFO: Successfully downloaded all records of the object '{}' to S3".format(gbl_objects.objectname))
                    break
        return False
    except AttributeError as e:
        print (e)
    except:
        e=sfdc_exception()
        #print("ERROR")
        return None, e
def getdata_rationalized(df,l_field_metadata):
    try:
        print("INFO: - Rationalizing date, datetime, double values")
        for col in l_field_metadata:
            if col[gbl_objects.fielddatatype] == 'date':
                df = df.withColumn(col[gbl_objects.objectfieldname],to_timestamp(df[col[gbl_objects.objectfieldname]],"yyyy-MM-dd"))
                #df = df.withColumn(col[gbl_objects.objectfieldname],to_timestamp(df[col[gbl_objects.objectfieldname]],gbl_objects.df_date_format))
            elif col[gbl_objects.fielddatatype]=='dateTime':
                df = df.withColumn(col[cc.objectfieldname],to_timestamp(df[col[gbl_objects.objectfieldname]],"yyyy-MM-dd'T'HH.mm.ss.SSSZ"))
                #df = df.withColumn(col[cc.objectfieldname],to_timestamp(df[col[gbl_objects.objectfieldname]],gbl_objects.df_datetime_format))
            elif col[gbl_objects.fielddatatype]=='double':
                df = df.withColumn(col[cc.objectfieldname],df[col[gbl_objects.objectfieldname]].cast(DoubleType()))
        return df, False
    except:
        e=sfdc_exception()
        #print ("ERROR - e")
        return None, e
def sfdc_getsoqlqueryurl (logger,l_field_names,schema):
    try:
        print("INFO: SOQL Query Started:")
        soql_fields = ','.join(l_field_names)
        if gbl_objects.filter_clause is None:
            soqlquery="SELECT "+ soql_fields +" from "+ gbl_objects.objectname+""
        else:
            soqlquery="SELECT "+ soql_fields +" from "+ gbl_objects.objectname+" where "+gbl_objects.filter_clause
        
        request_url = gbl_objects.baseurl + "/"+ gbl_objects.url_for_data + soqlquery
        print("INFO: Requested URL '{}' of soql query for object '{}'".format(request_url,gbl_objects.objectname))
        return request_url
    except:
        e = sfdc_exception()
        #print("ERROR:")
        return e
def sfdc_getmetadata_v2(logger):
    try:
        print ("INFO: Metadata started")
        #mck_Brick_to_Terr_External_ID__c, Name, LastName, FirstName, Salutation, RecordTypeId
        
        """
        data = {
            "Account":" id, territory_vod__c", 
            "Address_vod__c":" ACCOUNT,BRICK_VOD__C,ID,INCLUDE_IN_TERRITORY_ASSIGNMENT_VOD__C",
            "Brick_to_Terr_vod__c": "Name, TERRITORY_VOD__C"
            }
        object_attr_str = gbl_objects.object_attributes.replace("'", "\"")
        data = json.loads(object_attr_str)
        #data = json.loads[gbl_objects.object_attributes]
        selective_field_text = data[gbl_objects.objectname].lower()
        """
        object_flags = gbl_objects.object_flags
        selective_field_text = attribute_value_extract("columns").lower()
        
        selective_fields = [x.strip() for x in selective_field_text.split(",")]
        
        sfdc_auth_key, e = sfdc_access_token()
        loginurl = gbl_objects.baseurl + "/" + gbl_objects.url_for_metadata
        request_url = str(loginurl).replace('{objectname}', gbl_objects.objectname)
        sfdc_headers = {'Authorization': 'Bearer ' + sfdc_auth_key,
                        'Cache-Control': 'no-cache'}
        
        response = requests.get(request_url, headers=sfdc_headers)
        
        print("INFO: API respone status code '{}' Metadata".format(response.status_code))
        print("INFO: Requested URL '{}' of metadata for object '{}'".format(request_url,gbl_objects.objectname))
        if response.status_code > 300:
            error_msg = "ERROR: Error in connecting to SFDC application from '{}' - Metadata".format(gbl_objects.objectname)
            raise AttributeError(error_msg)
        else:
            d_object_desc = response.json()
            l_field_metadata = []
            l_field_names = []
            for json_key, json_value in d_object_desc.items():
                if json_key =="fields":
                    for field_details in json_value:
                        field_name = field_details.get(gbl_objects.fieldname)
                        if field_name.lower() in selective_fields:
                            field_data_type = field_details.get(gbl_objects.fieldsoaptype) [4:]
                            field_length=field_details.get(gbl_objects.field_length)
                            field_precision = field_details.get(gbl_objects.field_precision)
                            field_scale=field_details.get(gbl_objects.field_scale)
                            l_field_metadata.append([field_name,field_data_type,field_length,field_precision,field_scale])
                            l_field_names.append(field_name)
                    schema = sfdc_getsparkschema(l_field_metadata)
                    print ("INFO: l_field_names {}".format(l_field_names))
                    print ("INFO: Metadata extract successfully completed for '{}'.".format(gbl_objects.objectname))
                    return l_field_names,l_field_metadata, schema, False
    except AttributeError as e:
        error_msg = "ERROR : details were not fetched from SFDC"
        return None,None,None, e
    except:
        e=sfdc_exception()
        return None,None,None, e
def sfdc_getmetadata():
    try:
        print ("INFO: Metadata started")
        #mck_Brick_to_Terr_External_ID__c, Name, LastName, FirstName, Salutation, RecordTypeId
        
        """
        data = {
            "Account":" id, territory_vod__c", 
            "Address_vod__c":" ACCOUNT,BRICK_VOD__C,ID,INCLUDE_IN_TERRITORY_ASSIGNMENT_VOD__C",
            "Brick_to_Terr_vod__c": "Name, TERRITORY_VOD__C"
            }
        
        """
        object_attr_str = gbl_objects.object_attributes.replace("'", "\"")
        data = json.loads(object_attr_str)
        #data = json.loads[gbl_objects.object_attributes]
        selective_field_text = data[gbl_objects.objectname].lower()
        selective_fields = [x.strip() for x in selective_field_text.split(",")]
        
        sfdc_auth_key, e = sfdc_access_token()
        loginurl = gbl_objects.baseurl + "/" + gbl_objects.url_for_metadata
        request_url = str(loginurl).replace('{objectname}', gbl_objects.objectname)
        sfdc_headers = {'Authorization': 'Bearer ' + sfdc_auth_key,
                        'Cache-Control': 'no-cache'}
        
        response = requests.get(request_url, headers=sfdc_headers)
        
        print("INFO: API respone status code '{}' Metadata".format(response.status_code))
        print("INFO: Requested URL '{}' of metadata for object '{}'".format(request_url,gbl_objects.objectname))
        if response.status_code > 300:
            error_msg = "ERROR: Error in connecting to SFDC application from '{}' - Metadata".format(gbl_objects.objectname)
            raise AttributeError(error_msg)
        else:
            d_object_desc = response.json()
            l_field_metadata = []
            l_field_names = []
            for json_key, json_value in d_object_desc.items():
                if json_key =="fields":
                    for field_details in json_value:
                        field_name = field_details.get(gbl_objects.fieldname)
                        if field_name.lower() in selective_fields:
                            field_data_type = field_details.get(gbl_objects.fieldsoaptype) [4:]
                            field_length=field_details.get(gbl_objects.field_length)
                            field_precision = field_details.get(gbl_objects.field_precision)
                            field_scale=field_details.get(gbl_objects.field_scale)
                            l_field_metadata.append([field_name,field_data_type,field_length,field_precision,field_scale])
                            l_field_names.append(field_name)
                    schema = sfdc_getsparkschema(l_field_metadata)
                    print ("INFO: l_field_names {}".format(l_field_names))
                    print ("INFO: Metadata extract successfully completed for '{}'.".format(gbl_objects.objectname))
                    return l_field_names,l_field_metadata, schema, False
    except AttributeError as e:
        error_msg = "ERROR : details were not fetched from SFDC"
        return None,None,None, e
    except:
        e=sfdc_exception()
        return None,None,None, e
def sfdc_getsparkschema (l_field_metadata):
    try:
        print ("INFO: Sparkschema mapping started")
        map_type_sf_to_spark = {"boolean":"StringType",
                    "date":"StringType",
                    "dateTime":"StringType",
                    "double":"DoubleType",
                    "ID":"StringType",
                    "int":"IntegerType",
                    "string":"StringType",
                    "address":"StringType"
                    }
        l_schema_fields=[]
        for list in l_field_metadata:
            l_schema_fields.append(StructField(list[gbl_objects.objectfieldname],getattr(pyspark.sql.types, map_type_sf_to_spark[list[gbl_objects.fielddatatype]])(),True))
        schema = StructType(l_schema_fields)
        print ("INFO: Sparkschema mapping Completed")
        return schema
    except:
        e = sfdc_exception()
        #print(e)
        return e
def sfdc_access_token():
    try:
        sfdc_parameters = {"grant_type":gbl_objects.grant_type,
        "client_id":gbl_objects.client_id,
        "client_secret":gbl_objects.client_secret,
        "username":gbl_objects.user_id,
        "password":gbl_objects.password}
        
        login_url = gbl_objects.baseurl + "/" +gbl_objects.url_for_token
        response = requests.post(login_url, params=sfdc_parameters)
        
        print ("INFO: API response code while generating access token is '{}'".format(response.status_code))
        if response.status_code > 300:
            error_msg = "ERROR : Unable to access token for URL "
            raise AttributeError(error_msg)
        else:
            print("INFO: Generated access token successfully for URL - '{}'".format(login_url))
            return response.json().get("access_token"), False
    except AttributeError as e:
        print(e)
    except:
        e = sfdc_exception()
        return None, e
    
def main():
    # Passing Arguements
    args = getResolvedOptions(sys.argv, ['bucket','control_file_path','objectname','filter_key'])
    start_time = datetime.datetime.now()
    bucket = args ['bucket']
    key = args ['control_file_path']
    objectname = args ['objectname']
    filter_key = args ['filter_key']
    gbl_objects.objectname = objectname
    gbl_objects.filter_key = filter_key
    
    #Reading control file
    e = read_control_file (bucket,key)
    
    if e:
        print (e)
        return
   
    #Creating spark data frame from the glue context
    sc = pyspark.SparkContext()
    glueContext = GlueContext(sc)
    glue_spark = glueContext.spark_session
    job = Job(glueContext)
    
    logger = glueContext.get_logger()
    
    ## SFDC Process Initiated
    logger.info("INFO: SFDC Process Initiated")
    sfdc_process(glue_spark,logger)
    logger.info("INFO: SFDC Process Completed")
    end_time = datetime.datetime.now()
    print ("Object '{}' download completed within duration {} ".format(objectname,end_time-start_time))
    print ("Starts @ : {} ".format(start_time))
    print ("Ends @ : {} ".format(end_time))
    job.commit()
    return
###Ends the code
if __name__ == "__main__":
    main()
    