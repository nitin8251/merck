#------------------------------------------------------------------------------------------------------------------------------------------
# Purpose           : Data Processing - SFDC - Athena or AWS Glue Job.
# Environment       : MERCK-DEV-ICONNET
# Created by        : INFOSYS
# Created date      : 2021-02-11
# Description       : The Complete code available for data processing in AWS Glue Job 
#------------------------------------------------------------------------------------------------------------------------------------------
# Modification History
#------------------------------------------------------------------------------------------------------------------------------------------
# Modified By       : NA
# Modified Date     : NA
# Description       : NA
#------------------------------------------------------------------------------------------------------------------------------------------
# Modified By       : NA
# Modified Date     : NA
# Description       : NA
#------------------------------------------------------------------------------------------------------------------------------------------
import sys
import boto3
import datetime
import json
import linecache
import pyspark

from pyspark.sql import SparkSession
import pyspark.sql.functions as f
from collections import OrderedDict
from pyspark.sql.functions import udf, col, split,lit, concat
from pyspark.sql.types import ArrayType, IntegerType,StringType, NullType, StructType, StructField


from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue.utils import getResolvedOptions

class SFDC_DATA_PROCESSING_ERROR (Exception):
    def __init__(self, m):
        self.message = m
    def __str__(self):
        return self.message
class gbl_objects:
    s3_bucket = None
    s3_data_current = None
    s3_data_timestamp = None
    #object_attributes = None
    object_flags = None
    write_file_format = None
    using_athena = None
    run_query = None
    s3_result_current = None
    s3_result_timestamp = None
    s3_tmp_path = None
    s3_intermediate_path = None
    result_file_format = None
    default_no_partitions =None
    operation_mode = None
    result_name= None
    
def sfdc_exception(Debug=True):
    exc_type, exc_obj,tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    file_name = f.f_code.co_filename
    linecache.checkcache(file_name)
    line = linecache.getline(file_name, lineno, f.f_globals)
    error_obj = {'error_code':str(exc_type),'error_message':str(exc_obj)}
    #print 'ERROR: EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj)
    error_obj['error_at']="ERROR: EXCEPTION IN ('{}', LINE '{}' '{}')".format(file_name, lineno, line.strip())
    format_error_msg = "ERROR CODE : {}; REASON :{}; LINE_AT : {} ".format(error_obj['error_code'],error_obj['error_message'],error_obj['error_at'])
    raise SFDC_DATA_PROCESSING_ERROR (format_error_msg)
    return error_obj

def read_control_file (logger,bucket,key):
    try:
    
        s3obj = boto3.client('s3')
        #bucket = 'iconnectuser' 
        #key = 'scripts/move_file/app/control.json'
        s3_file = s3obj.get_object(Bucket = bucket , Key = key) 
        params = s3_file['Body'].read().decode('utf-8')
        control_file = json.loads(params)
    
        gbl_objects.s3_bucket = control_file["s3_bucket"]
        gbl_objects.s3_data_current = control_file["s3_data_current"]
        gbl_objects.s3_data_timestamp  = control_file["s3_data_timestamp"]
        #gbl_objects.object_attributes  = control_file["object_attributes"]
        gbl_objects.object_flags = control_file["object_flags"]
        gbl_objects.write_file_format  = control_file["write_file_format"]
        
        gbl_objects.run_query  = control_file["run_query"]
        gbl_objects.s3_result_current  = control_file["s3_result_current"]
        gbl_objects.s3_result_timestamp  = control_file["s3_result_timestamp"]
        gbl_objects.s3_tmp_path  = control_file["s3_tmp_path"]
        gbl_objects.s3_intermediate_path  = control_file["s3_intermediate_path"]
        gbl_objects.result_file_format  = control_file["result_file_format"]
        gbl_objects.default_no_partitions = control_file["default_no_partitions"]
        gbl_objects.operation_mode = control_file["operation_mode"]
        gbl_objects.result_name = control_file["result_name"]
        
        
        logger.info("SFDC - read_control_file successfully completed ")
        return False
    except:
        e=sfdc_exception()
        return e
def attribute_value_extract(key):
    try:
        #key = "active_flag"
        value=key
        object_flags = gbl_objects.object_flags
        
        #selective_field_text = object_flags[gbl_objects.objectname]["columns"].lower()
        for attribute in object_flags[gbl_objects.objectname]:
            if key == attribute:
               value=object_flags[gbl_objects.objectname][key]
               break;
        return value
    except:
        e=sfdc_exception()
        return e
def attribute_value_extract_operation(key):
    try:
        #key = "active_flag"
        value=key
        operation_mode = gbl_objects.operation_mode
        
        #selective_field_text = object_flags[gbl_objects.objectname]["columns"].lower()
        for attribute in operation_mode:
            if key == attribute:
               value= operation_mode[key]
               break;
        return value
    except:
        e=sfdc_exception()
        return e
def data_serialization_process(spark,tmp_df1):
    try:
        tmp_df2 = tmp_df1.groupby("Id","mck_Primary_Country__c").agg(f.concat_ws("", f.collect_list(tmp_df1.Territory_vod__c)).alias("tmp_con_terr_1"))
        tmp_df3 = tmp_df2.select(col("id"),col("mck_Primary_Country__c"), split(col("tmp_con_terr_1"), ";").alias("tmp_con_terr_2"))
        
        fn_drop_duplicates_elements = udf(lambda row: list(set(row)), ArrayType(StringType()))
        
        #tmp_df4 = tmp_df3.withColumn("tmp_con_terr_3", fn_drop_duplicates_elements("tmp_con_terr_2"))
        #tmp_df5 = tmp_df4.withColumn("Territory_vod__c", f.concat_ws(";", "tmp_con_terr_3")).select("Id","Territory_vod__c")
        
        tmp_df4 = tmp_df3.withColumn("Territory_vod__c", f.concat_ws(";",fn_drop_duplicates_elements("tmp_con_terr_2"))).select("Id","Territory_vod__c","mck_Primary_Country__c")
        
        
        tmp_df5 = tmp_df4.select("Id",concat(tmp_df4['Territory_vod__c'], lit(';')).alias('Territory_vod__c'))
        
        
        return tmp_df5
    except:
        e=sfdc_exception()
        return e
def sfdc_data_processing(logger,spark):
    try:
        # Read 3 curren_path objects
        delta_records = "No"
        no_partitions = int(gbl_objects.default_no_partitions)
        write_file_format = gbl_objects.write_file_format
        
        
        run_query = gbl_objects.run_query
        s3_result_current = gbl_objects.s3_result_current
        s3_result_timestamp = gbl_objects.s3_result_timestamp
        s3_tmp_path = gbl_objects.s3_tmp_path
        s3_intermediate_path = gbl_objects.s3_intermediate_path
        result_file_format = gbl_objects.result_file_format
        
        now = datetime.datetime.today() 
        timestamp_path = now.strftime("%Y-%m-%d/%H-%M-%S")
        s3_result_current = s3_result_current.replace("{s3_bucket}",gbl_objects.s3_bucket)
        s3_result_timestamp = s3_result_timestamp.replace("{s3_bucket}",gbl_objects.s3_bucket)
        s3_result_timestamp = s3_result_timestamp.replace("{timestamp}",timestamp_path)
        s3_tmp_path = s3_tmp_path.replace("{s3_bucket}",gbl_objects.s3_bucket)
        s3_intermediate_path = s3_intermediate_path.replace("{s3_bucket}",gbl_objects.s3_bucket)
        
        gbl_objects.s3_result_current = s3_result_current
        gbl_objects.s3_result_timestamp = s3_result_timestamp 
        gbl_objects.s3_tmp_path = s3_tmp_path
        gbl_objects.s3_intermediate_path = s3_intermediate_path
        
        """
        object_attributes_str = gbl_objects.object_attributes.replace("'", "\"")
        data = json.loads(object_attributes_str)
        """
        data = gbl_objects.object_flags
        for objectname, object_value in data.items():
            s3_path = gbl_objects.s3_data_current
            #print(objectname)
            s3_path = s3_path.replace("{s3_bucket}",gbl_objects.s3_bucket)
            s3_path = s3_path.replace("{objectname}",objectname)
            data[objectname] = s3_path

        print ("")
        print (data)
        
        Acc_schema = StructType([StructField("Id", StringType(), True),
                     StructField("Territory_vod__c", StringType(), True)
                     ]
                     )
        for objectname, s3_path in data.items():
            if objectname == 'Address_vod__c' and write_file_format == 'parquet':
                df1 = spark.read.parquet(s3_path)
            if objectname == 'Brick_to_Terr_vod__c' and write_file_format == 'parquet': 
                df2 = spark.read.parquet(s3_path)
            if objectname == 'Account' and write_file_format == 'parquet': 
                try:
                    df3 = spark.read.parquet(s3_path)
                except:
                    df3 = spark.createDataFrame([], Acc_schema) 
            """
            if objectname == 'Address_vod__c' and write_file_format == 'json':
                df1 = spark.read.json(s3_path)
            if objectname == 'Brick_to_Terr_vod__c' and write_file_format == 'json': 
                df2 = spark.read.json(s3_path)
            if objectname == 'Address_vod__c' and write_file_format == 'csv':
                df1 = spark.read.csv(s3_path)
            if objectname == 'Brick_to_Terr_vod__c' and write_file_format == 'csv': 
                df2 = spark.read.csv(s3_path)
            """
        
        df1.registerTempTable("address")
        df2.registerTempTable("brick_to_terr")
        df3.registerTempTable("account")
        
        tmp_df = spark.sql(run_query)
        if delta_records.lower() == "yes":
            #tmp_df1.printSchema()
            target_df = data_serialization_process(spark,tmp_df)
            #df3.printSchema()
            #source_df = data_serialization_process(spark,df3)
            source_df = df3
            result_df = target_df.join(source_df,on=['Id','Territory_vod__c'],how='left_anti')
            print("Delta data processing with comparing Account Object")
            logger.info("Delta data processing with comparing Account Object")
        else:
            result_df   = data_serialization_process (spark,tmp_df)
            print("Full data processing without comparing Account Object")
            logger.info("Full data processing without comparing Account Object")
        result_df.coalesce(no_partitions).write.format(result_file_format).mode('overwrite').save(s3_result_current)
        no_of_records = result_df.count()
        print("# of Records : {}. Result has been stored in '{}' with '{}' format ".format(no_of_records,s3_result_current,result_file_format))
        logger.info("# of Records : {}. Result has been stored in '{}' with '{}' format ".format(no_of_records,s3_result_current,result_file_format))
        return False
    except:
        e=sfdc_exception()
        return e
def sfdc_data_processing_v2(logger,spark):
    try:
        # Read 3 curren_path objects
        # delta_records = "No" 
        delta_records = attribute_value_extract_operation ("delta_load_with_current_vs_account").lower()
        no_partitions = int(gbl_objects.default_no_partitions)
        write_file_format = gbl_objects.write_file_format
        
        
        run_query = gbl_objects.run_query
        s3_result_current = gbl_objects.s3_result_current
        s3_result_timestamp = gbl_objects.s3_result_timestamp
        s3_tmp_path = gbl_objects.s3_tmp_path
        s3_intermediate_path = gbl_objects.s3_intermediate_path
        result_file_format = gbl_objects.result_file_format
        operation_mode = gbl_objects.operation_mode
        now = datetime.datetime.today() 
        timestamp_path = now.strftime("%Y-%m-%d/%H-%M-%S")
        s3_result_current = s3_result_current.replace("{s3_bucket}",gbl_objects.s3_bucket)
        s3_result_timestamp = s3_result_timestamp.replace("{s3_bucket}",gbl_objects.s3_bucket)
        s3_result_timestamp = s3_result_timestamp.replace("{timestamp}",timestamp_path)
        s3_tmp_path = s3_tmp_path.replace("{s3_bucket}",gbl_objects.s3_bucket)
        s3_intermediate_path = s3_intermediate_path.replace("{s3_bucket}",gbl_objects.s3_bucket)
        s3_result_current = s3_result_current.replace("{result_name}",gbl_objects.result_name)
        s3_result_timestamp = s3_result_timestamp.replace("{result_name}",gbl_objects.result_name)
        
        s3_tmp_full_path =  s3_tmp_path + gbl_objects.result_name+"/full/"
        s3_tmp_delta_path = s3_tmp_path + gbl_objects.result_name+"/delta/"
        
        gbl_objects.s3_result_current = s3_result_current
        gbl_objects.s3_result_timestamp = s3_result_timestamp 
        gbl_objects.s3_tmp_path = s3_tmp_path
        gbl_objects.s3_intermediate_path = s3_intermediate_path
        
        """
        object_attributes_str = gbl_objects.object_attributes.replace("'", "\"")
        data = json.loads(object_attributes_str)
        """
        data = gbl_objects.object_flags
        for objectname, object_value in data.items():
            s3_path = gbl_objects.s3_data_current
            #print(objectname)
            s3_path = s3_path.replace("{s3_bucket}",gbl_objects.s3_bucket)
            s3_path = s3_path.replace("{objectname}",objectname)
            data[objectname] = s3_path

        print ("")
        print (data)
        
        Acc_schema = StructType([StructField("Id", StringType(), True),
                     StructField("Territory_vod__c", StringType(), True)
                     ]
                     )
        for objectname, s3_path in data.items():
            if objectname == 'Address_vod__c' and write_file_format == 'parquet':
                df1 = spark.read.parquet(s3_path)
            if objectname == 'Brick_to_Terr_vod__c' and write_file_format == 'parquet': 
                df2 = spark.read.parquet(s3_path)
            if objectname == 'Account' and write_file_format == 'parquet': 
                try:
                    df3 = spark.read.parquet(s3_path)
                except:
                    df3 = spark.createDataFrame([], Acc_schema) 
            """
            if objectname == 'Address_vod__c' and write_file_format == 'json':
                df1 = spark.read.json(s3_path)
            if objectname == 'Brick_to_Terr_vod__c' and write_file_format == 'json': 
                df2 = spark.read.json(s3_path)
            if objectname == 'Address_vod__c' and write_file_format == 'csv':
                df1 = spark.read.csv(s3_path)
            if objectname == 'Brick_to_Terr_vod__c' and write_file_format == 'csv': 
                df2 = spark.read.csv(s3_path)
            """
        
        df1.registerTempTable("address")
        df2.registerTempTable("brick_to_terr")
        df3.registerTempTable("account")
        
        tmp_df = spark.sql(run_query)
        if delta_records.lower() == "yes":
            #tmp_df1.printSchema()
            target_df = data_serialization_process(spark,tmp_df)
            #df3.printSchema()
            #source_df = data_serialization_process(spark,df3)
            source_df = df3
            result_df = target_df.join(source_df,on=['Id','Territory_vod__c'],how='left_anti')
            _count = result_df.count()
            target_df.coalesce(no_partitions).write.format(result_file_format).mode('overwrite').save(s3_tmp_full_path)
            print("Delta data processing by comparing Account Object {}".format(_count))
            logger.info("Delta data processing by comparing Account Object")
            
        else:
            result_df   = data_serialization_process (spark,tmp_df)
            result_df.coalesce(no_partitions).write.format(result_file_format).mode('overwrite').save(s3_tmp_full_path)
            print("Full data processing without comparing Account Object")
            logger.info("Full data processing without comparing Account Object")
            
        # Full mode Vs Delta mode (Compare with Previous Load)
        
        
        
        
        
        #print (attribute_value_extract_operation ("delta_load_vs_previous_data").lower())
        
        if attribute_value_extract_operation ("delta_load_with_current_vs_previous_data").lower() == "yes":
            if result_file_format == "csv":
                try:
                    s3_result_current = s3_result_current.replace("{load_type}","full")
                    previous_df = spark.read.csv(s3_result_current,header=False,schema=Acc_schema)
                except:
                    previous_df = spark.createDataFrame([], Acc_schema) 
                 
            if result_file_format == "parquet":
                try:
                    previous_df = spark.read.parquet(s3_result_current)
                except:
                    previous_df = spark.createDataFrame([], Acc_schema) 
            
            delta_df = result_df.join(previous_df,on=['Id','Territory_vod__c'],how='left_anti')
            _no_of__previous_records = previous_df.count()
            if _no_of__previous_records > 0: 
                print("Changed data ONLY by comparing previous successful load")
                logger.info("Changed data ONLY by comparing previous successful load")
            else:
                print("Full data - since there is no previous successful load")
                logger.info("Full data - since there is no previous successful load")
        else:
            delta_df = result_df
            print("Full data - without comparing previous successful load")
            logger.info("Full data - without comparing previous successful load")
            
        delta_df.coalesce(no_partitions).write.format(result_file_format).mode('overwrite').save(s3_tmp_delta_path)
        
        no_of_records = delta_df.count()
        print("# of Records : {}. Result has been stored in '{}' with '{}' format ".format(no_of_records,s3_tmp_delta_path,result_file_format))
        logger.info("# of Records : {}. Result has been stored in '{}' with '{}' format ".format(no_of_records,s3_tmp_delta_path,result_file_format))
        return False
    except:
        e=sfdc_exception()
        return e
def main():
    print(1/0)
    args = getResolvedOptions(sys.argv, ['bucket','control_file_path'])
    start_time = datetime.datetime.now()
    bucket = args ['bucket']
    key = args ['control_file_path']

    
    sc = pyspark.SparkContext()
    glueContext = GlueContext(sc)
    
    logger = glueContext.get_logger()
    print("SFDC - Data Processing started")
    logger.info("SFDC - Data Processing started")
    #logger.warn("warn message")
    #logger.error("error message")
    #control_file_path = 'scripts/move_file/app/control.json'
    #gbl_objects.objectname = objectname
    #gbl_objects.operation = operation
    #Reading control file
    e = read_control_file (logger,bucket,key)
    if e:
        print (e)
        return
    #data processing starts 
    e = sfdc_data_processing_v2(logger,glueContext)
    if e:
        print (e)
        return
    
    print("SFDC - Data Processing completed")
    logger.info("SFDC - Data Processing completed")
    end_time = datetime.datetime.now()
    print ("Data processing completed for duration {} ".format(end_time-start_time))
    print ("Starts @ : {} ".format(start_time))
    print ("Ends @ : {} ".format(end_time))
    return
if __name__ == "__main__":
    main()