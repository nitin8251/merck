#------------------------------------------------------------------------------------------------------------------------------------------
# Purpose           : SFDC Account Data Update.
# Environment       : MERCK-DEV-ICONNET
# Created by        : INFOSYS
# Created date      : 2021-02-23
# Description       : SFDC Account Data Update 
#------------------------------------------------------------------------------------------------------------------------------------------
# Modification History
#------------------------------------------------------------------------------------------------------------------------------------------
# Modified By       : NA
# Modified Date     : NA
# Description       : NA
#------------------------------------------------------------------------------------------------------------------------------------------
# Modified By       : NA
# Modified Date     : NA
# Description       : NA
#------------------------------------------------------------------------------------------------------------------------------------------
import sys
import requests
import datetime
import json
import linecache
import logging
import boto3
import base64
import io
import re

import pandas as pd
import numpy

from awsglue.utils import getResolvedOptions
from simple_salesforce import Salesforce, SalesforceLogin

class SFDC_DATA_UPDATE_ERROR (Exception):
    def __init__(self, m):
        self.message = m
    def __str__(self):
        return self.message
class gbl_objects:
    s3_bucket = None
    baseurl = None
    user_id = None
    password = None
    client_id = None
    client_secret = None
    grant_type = None
    url_for_token = None
    url_for_metadata = None
    url_for_data = None
    s3_result_current = None
    s3_tmp_path = None
    objectname = None
    parallel_batch_size = None
    result_name = None
    s3_result_timestamp = None
def sfdc_encrypt (password):
    password = password.encode('utf-8')
    encoded_text = base64.b64encode(password)
    return encoded_text.decode('utf-8')


def sfdc_decrypt (password):
    password = password.encode('utf-8')
    decoded_text = base64.b64decode(password)
    return decoded_text.decode('utf-8')
def read_control_file (bucket,key):
    try:
    
        s3obj = boto3.client('s3') 
        #bucket = 'iconnectuser' 
        #key = 'scripts/move_file/app/control.json'
        s3_file = s3obj.get_object(Bucket = bucket , Key = key) 
        params = s3_file['Body'].read().decode('utf-8')
        control_file = json.loads(params)
    
        gbl_objects.s3_bucket = control_file["s3_bucket"]
        gbl_objects.baseurl = control_file["baseurl"]
        gbl_objects.user_id = control_file["user_id"]
        gbl_objects.password = sfdc_decrypt(control_file["password"]) #sfdc_decrypt(enc_password)
        gbl_objects.client_id = control_file["client_id"]
        gbl_objects.client_secret = control_file["client_secret"]
        gbl_objects.grant_type = control_file["grant_type"]
        gbl_objects.url_for_token = control_file["url_for_token"]
        gbl_objects.s3_result_current = control_file["s3_result_current"]
        gbl_objects.s3_result_timestamp = control_file["s3_result_timestamp"]
        gbl_objects.s3_tmp_path = control_file["s3_tmp_path"]
        gbl_objects.objectname = ""
        gbl_objects.parallel_batch_size = control_file["parallel_batch_size"]
        gbl_objects.result_name = control_file["result_name"]
        
        
        return False
    except:
        e=sfdc_exception()
    return e
def sfdc_access_token():
    try:
        sfdc_parameters = {"grant_type":gbl_objects.grant_type,
        "client_id":gbl_objects.client_id,
        "client_secret":gbl_objects.client_secret,
        "username":gbl_objects.user_id,
        "password":gbl_objects.password}
        
        login_url = gbl_objects.baseurl + "/" +gbl_objects.url_for_token
        response = requests.post(login_url, params=sfdc_parameters)
        
        print ("INFO: API response code while generating access token is '{}'".format(response.status_code))
        if response.status_code > 300:
            error_msg = "ERROR : Unable to access token for URL "
            raise AttributeError(error_msg)
        else:
            print("INFO: Generated access token successfully for URL - '{}'".format(login_url))
            return response.json().get("access_token"), False
    except AttributeError as e:
        print(e)
    except:
        e = sfdc_exception()
        return None, e
def sfdc_exception(Debug=True):
    exc_type, exc_obj,tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    file_name = f.f_code.co_filename
    linecache.checkcache(file_name)
    line = linecache.getline(file_name, lineno, f.f_globals)
    error_obj = {'error_code':str(exc_type),'error_message':str(exc_obj)}
    #print 'ERROR: EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj)
    error_obj['error_at']="ERROR: EXCEPTION IN ('{}', LINE '{}' '{}')".format(file_name, lineno, line.strip())
    format_error_msg = "ERROR CODE : {}; REASON :{}; LINE_AT : {} ".format(error_obj['error_code'],error_obj['error_message'],error_obj['error_at'])
    raise SFDC_DATA_UPDATE_ERROR (format_error_msg)
    return error_obj
def s3_path_to_current(s3_path):
    s3_path = s3_path.replace("{s3_bucket}",gbl_objects.s3_bucket)
    s3_path = s3_path.replace("{objectname}",gbl_objects.objectname)
    #logger.info("SFDC - Current S3 path is '{}' ".format(s3_path))
    return s3_path
def s3_search_file (bucket,source_file_path, search_files):
    try:
        bucket_name = bucket
        prefix  = source_file_path.split(bucket+"/")[1]
    
        s3_conn = boto3.client('s3')  # type: BaseClient  ## again assumes boto.cfg setup, assume AWS S3
        s3_result = s3_conn.list_objects_v2(Bucket=bucket_name, Prefix=prefix, Delimiter = "/")
    
        if 'Contents' not in s3_result:
            #print(s3_result)
            return []
    
        file_list = []
        search_files = search_files.replace("*","")
        
        for filename in search_files.split(","):
            for key in s3_result['Contents']:
                if filename.strip() in key['Key']:
                    file_list.append(key['Key'])
                    print(key['Key'])
            print("List count = {}".format(len(file_list)))
            while s3_result['IsTruncated']:
                continuation_key = s3_result['NextContinuationToken']
                s3_result = s3_conn.list_objects_v2(Bucket=bucket_name, Prefix=prefix, Delimiter="/", ContinuationToken=continuation_key)
                for key in s3_result['Contents']:
                    if filename.strip() in key['Key']:
                        file_list.append(key['Key'])
                        print(key['Key'])
                    
                print("List count = {}".format(len(file_list)))
        
        return file_list
    except:
        e=sfdc_exception()
        #logger.error(e)
        #raise SFDC_DATA_UPDATE_ERROR(e)
    return e
def s3_read_csv(bucket,file_list):
    try:
        s3obj = boto3.client('s3') 
        prefix_df = pd.DataFrame()
        
        for key in file_list:
            s3_file = s3obj.get_object(Bucket=bucket, Key=key)
            try:
                temp_df = pd.read_csv(io.BytesIO(s3_file['Body'].read()), header=None)
            except:
                temp_df = pd.DataFrame()
            print ("TEMP_DF_COUNT = {}".format(temp_df.shape[0]))
            frames = [prefix_df, temp_df]
            prefix_df = pd.concat(frames)
            
        print ("PREFIX_DF_COUNT = {}".format(prefix_df.shape[0]))
        return prefix_df
    except:
        e = sfdc_exception()
        print (e)
        #raise SFDC_DATA_UPDATE_ERROR (e)
        return e
def s3_write_csv(bucket,pd_df,file_list):
    try:
        s3_result_error = gbl_objects.s3_result_timestamp
        result_name = gbl_objects.result_name
        s3_result_error = s3_result_error.replace("{s3_bucket}",bucket).replace("{result_name}",result_name).replace("{load_type}","error")
        s3_result_error  = s3_result_error.split(bucket+"/")[1]
        now = datetime.datetime.today() 
        timestamp_path = now.strftime("%Y-%m-%d")
        s3_result_error =s3_result_error.replace("{timestamp}",timestamp_path)
        
        _un_file_name = file_list[0].split("/")[-1].split(".")[0].replace("part","err_part")
        
        _un_file_name = s3_result_error + _un_file_name +".csv"
        
        csv_buffer = io.StringIO()
        pd_df.to_csv(csv_buffer,index = False)
        
        s3_resource = boto3.resource('s3')
        s3_resource.Object(bucket, _un_file_name).put(Body=csv_buffer.getvalue())
        print("INFO : Check the files under {}".format(_un_file_name))
        return
    except:
        e = sfdc_exception()
        print (e)
        #raise SFDC_DATA_UPDATE_ERROR (e)
        return e
    
def sfdc_data_update(bucket,search_files):
    try:
        #search_files = "00001,00009,00000,00008"
        #interval = 10000
        
        #SFDC Connection Establishment
        instance_url = gbl_objects.baseurl
        parallel_batch_size = int(gbl_objects.parallel_batch_size)
        security_token, e = sfdc_access_token()
        
        sfc = Salesforce(instance_url=instance_url, session_id=security_token)
        #csv_data = sfc.query("SELECT id, territory_vod__c FROM Account WHERE Id  in ('0012o00002WWJoXAAX','0012o00002XrgauAAB')")
        
        source_file_path = gbl_objects.s3_tmp_path + gbl_objects.result_name +"/delta/"
        #source_file_path = source_file_path.replace("{result_name}",gbl_objects.result_name)
        #source_file_path = source_file_path.replace("{load_type}","delta")
        source_file_path = s3_path_to_current(source_file_path)
        #gbl_objects.s3_result_current = source_file_path
        file_list = s3_search_file (bucket,source_file_path, search_files)
        
        print (file_list)
        if len(file_list) ==0:
            return
        
        df = s3_read_csv(bucket,file_list)
        _no_of_rows = df.shape[0]
        _no_of_cols = df.shape[1]
        if _no_of_rows <=0:
            return
        
        if _no_of_cols == 2:
            df.columns =['Id', 'Territory_vod__c']
        if _no_of_cols == 3:
            df.columns =['Id', 'Territory_vod__c', 'mck_Primary_Country__c']
        print("Total # of rows : {}".format(_no_of_rows))
        
        df = df.dropna()
        my_data = df.to_numpy()
        counter = 0
        
        formated_data = [{'Id':data[0],'Territory_vod__c': data[1]} for data in my_data[0:_no_of_rows]]
        #formated_data = [{'Id':data[0],'Territory_vod__c': data[1]} for data in my_data[0:50000]]
        #print (formated_data)
        upd_sts = sfc.bulk.Account.update(formated_data,batch_size=parallel_batch_size,use_serial=False)
        
        
        print(type(upd_sts))
        print(len(upd_sts))
        
        failed_list = list(filter(lambda x: x['success'] != True, upd_sts))
        
        while failed_list:
            failed_df = df
            #print (failed_list)
            print("Failed Ids - count # :")
            print(len(failed_list))
            failed_listed_ids = []
            un_processed_list = []
            
            ids_range_dict = {}
            for index, message in enumerate(failed_list):
                if message["errors"][0]["statusCode"] == "UNABLE_TO_LOCK_ROW":
                    x = message["errors"][0]["message"]
                    length = re.findall(r'\d+', x.split(":")[0].strip())[0]
                    start_id = x.split(":")[1].strip().split(",")[0]
                    #print (start_id)
                    #print (length)
                    failed_listed_ids.append([start_id,length])
                    ids_range_dict[start_id] = length
                    #failed_listed_ids.append(x.split(":")[1].strip().split(","))
                else:
                    un_processed_list.append(message["errors"])
            #failed_ids = list(set(list(numpy.concatenate(failed_listed_ids).flat)))
            
            print("Failed Ids # : ")
            print(failed_listed_ids)
            print("Failed Ids # in dictionary")
            print (ids_range_dict)
            #failed_df = failed_df[failed_df['Id'].isin(failed_ids)]
            if _no_of_cols == 2:
                _tmp_df = pd.DataFrame(columns=['Id', 'Territory_vod__c'])
            if _no_of_cols == 3:
                _tmp_df = pd.DataFrame(columns=['Id', 'Territory_vod__c','mck_Primary_Country__c'])
            for key_as_id, value_as_records in ids_range_dict.items():
                ind = failed_df.index[failed_df['Id'] == key_as_id].tolist()
                start_index = ind[0]
                end_index = ind[0] + int(value_as_records) #value_as_records
                _tmp_df1 = failed_df.iloc[start_index:end_index,:]
                _tmp_frames = [_tmp_df,_tmp_df1]
                _tmp_df = pd.concat(_tmp_frames)
    
    
             
            _failed_no_of_rows = _tmp_df.shape[0]
            failed_data = _tmp_df.to_numpy()
            failed_formated_data = [{'Id':failed_data[0],'Territory_vod__c': failed_data[1]} for failed_data in failed_data[0:_failed_no_of_rows]]
            failed_sfc = Salesforce(instance_url=instance_url, session_id=security_token)
            failed_upd_sts = failed_sfc.bulk.Account.update(failed_formated_data,batch_size=parallel_batch_size,use_serial=False)
            failed_list = list(filter(lambda x: x['success'] != True, failed_upd_sts))
            
            
            
            if len(un_processed_list) > 0:
                print("INFO : Errors captured other than 'TooManyLockFailure")
                #un_processed_df = pd.DataFrame(un_processed_list[1:],columns=un_processed_list[0])
                un_processed_df = pd.DataFrame(un_processed_list,columns=["Error_message"])
                s3_write_csv(bucket,un_processed_df,file_list)
                
            
        """
        while counter <= _no_of_rows - 1:
            start_index = counter
            if counter + interval > _no_of_rows:
                end_index = _no_of_rows - 1
            else:
                end_index = counter + interval - 1
            #print ("start index = {}".format(start_index))
            #print ("end index = {}".format(end_index))
            #print ("Counter index = {}".format(counter))
            formated_data = [{'Id':data[0],'Territory_vod__c': data[1]} for data in my_data[0:1000]]
            #formated_data = [{'Id':data[0],'Territory_vod__c': data[1]} for data in my_data[start_index:end_index]]
            sfc.bulk.Account.update(formated_data,batch_size=len(formated_data),use_serial=False)
            counter += interval
        #for row in csv_data:
        #    print(row)
        """
        print ("INFO: Function completed - sfdc_data_update")
        return None
    except:
        e = sfdc_exception()
        print (e)
        #raise SFDC_DATA_UPDATE_ERROR (e)
        return e
def main():
    # Passing Arguements
    args = getResolvedOptions(sys.argv, ['bucket','control_file_path','search_files'])
    start_time = datetime.datetime.now()
    bucket = args ['bucket']
    key = args ['control_file_path']
    search_files = args ['search_files']
    print("INFO: Account Data Update started for files '{}'".format(search_files))
    
    #Reading control file
    e = read_control_file (bucket,key)
    if e:
        print (e)
        return
    e = sfdc_data_update (bucket,search_files)
    if e:
        print (e)
        return
    print("INFO: Account Data Update completed for files '{}'".format(search_files))
    
    end_time = datetime.datetime.now()
    print ("INFO: Account Data Update completed for duration {} ".format(end_time-start_time))
    print ("Starts @ : {} ".format(start_time))
    print ("Ends @ : {} ".format(end_time))
    
    return
if __name__ == "__main__":
    main()