import sys
import base64
#from awsglue.context import GlueContext
#from awsglue.job import Job
from awsglue.utils import getResolvedOptions

def sfdc_encrypt (password):
    password = password.encode('utf-8')
    encoded_text = base64.b64encode(password)
    return encoded_text.decode('utf-8')


def sfdc_decrypt (password):
    password = password.encode('utf-8')
    decoded_text = base64.b64decode(password)
    return decoded_text.decode('utf-8')


def main():
    
    args = getResolvedOptions(sys.argv, ['password'])
    #start_time = datetime.datetime.now()
    password = args ['password']

    print ("Password is {}".format(password))
    
    enc_password = sfdc_encrypt(password)
    
    dec_password = sfdc_decrypt(enc_password)
    print ("\nEncrypted - Password is \n{}".format(enc_password))
    print ("\nDecrypted - Password is \n{}".format(dec_password))
    return

if __name__ == "__main__":
    main()