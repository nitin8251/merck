
#------------------------------------------------------------------------------------------------------------------------------------------
# Purpose           : SFDC GLUE_JOB PARAMETER PASSING.
# Environment       : MERCK-DEV-ICONNET
# Created by        : INFOSYS
# Created date      : 2021-03-05
# Description       : Workflow.json for parameter arguements of Glue Jobs
#------------------------------------------------------------------------------------------------------------------------------------------
# Modification History
#------------------------------------------------------------------------------------------------------------------------------------------
# Modified By       : NA
# Modified Date     : NA
# Description       : NA
#------------------------------------------------------------------------------------------------------------------------------------------
# Modified By       : NA
# Modified Date     : NA
# Description       : NA
#------------------------------------------------------------------------------------------------------------------------------------------


import sys
from awsglue.utils import getResolvedOptions
import boto3
import time
import linecache
import json
import re

class SFDC_GLUE_JOB_ERROR (Exception):
    def __init__(self, m):
        self.message = m
    def __str__(self):
        return self.message
        
class gbl_objects:
    file_name = None
    version = None
    service_name = None
    region_name = None
    endpoint_url = None
    bucketname = None
    control_file_path = None
    comments = None
    iconnect_jobs = None
    invoked_job_name = None

def sfdc_exception(Debug=True):
    exc_type, exc_obj,tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    file_name = f.f_code.co_filename
    linecache.checkcache(file_name)
    line = linecache.getline(file_name, lineno, f.f_globals)
    error_obj = {'error_code':str(exc_type),'error_message':str(exc_obj)}
    #print 'ERROR: EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj)
    error_obj['error_at']="ERROR: EXCEPTION IN ('{}', LINE '{}' '{}')".format(file_name, lineno, line.strip())
    format_error_msg = "ERROR CODE : {}; REASON :{}; LINE_AT : {} ".format(error_obj['error_code'],error_obj['error_message'],error_obj['error_at'])
    raise SFDC_GLUE_JOB_ERROR (format_error_msg)
    return error_obj
def read_control_file (invoked_job_name,bucket,workflow_control_file_path):
    try:
        s3obj = boto3.client('s3') 
        s3_file = s3obj.get_object(Bucket = bucket , Key = workflow_control_file_path) 
        params = s3_file['Body'].read().decode('utf-8')
        control_file = json.loads(params)
        gbl_objects.invoked_job_name = invoked_job_name
        gbl_objects.file_name = control_file["file_name"]
        gbl_objects.version = control_file["version"]
        gbl_objects.service_name  = control_file["service_name"]
        gbl_objects.region_name  = control_file["region_name"]
        gbl_objects.endpoint_url  = control_file["endpoint_url"]
        gbl_objects.bucketname  = control_file["bucketname"]
        gbl_objects.control_file_path  = control_file["control_file_path"]
        gbl_objects.comments  = control_file["comments"]
        
        print (gbl_objects.invoked_job_name)
        gbl_objects.iconnect_jobs  = control_file["iconnect_jobs"]
        return False
    except:
        e = sfdc_exception()
        return e
def pass_arguements_jobs():
    try:
        invoked_job_name = gbl_objects.invoked_job_name
        file_name = gbl_objects.file_name
        version = gbl_objects.version
        service_name = gbl_objects.service_name
        region_name = gbl_objects.region_name
        endpoint_url = gbl_objects.endpoint_url
        bucketname = gbl_objects.bucketname
        control_file_path = gbl_objects.control_file_path
        comments = gbl_objects.comments
        iconnect_jobs = gbl_objects.iconnect_jobs
        
        print ("Invoked Job Name = {} ".format(invoked_job_name))
        print("Comments = {}".format(comments))
        parameter_dict = iconnect_jobs[invoked_job_name]
        globals().update(parameter_dict)
        _base_job_name = Base_Job_Name
        _arguements = {"--" + str(key): val.replace("{bucketname}",bucketname).replace("{control_file_path}",control_file_path) for key, val in Arguements.items()}  
        
        print ("Base Job Name = {}".format(_base_job_name))
        print ("Arguements = ")
        print (_arguements)
        # Job Starts
        #return
        jobname = _base_job_name
        arguments = _arguements
        client = boto3.client(service_name=service_name, region_name=region_name,
                  endpoint_url=endpoint_url) 
        response = client.start_job_run(JobName=jobname,Arguments=arguments)
        status = client.get_job_run(JobName=jobname, RunId=response['JobRunId'])
        
        if status:
            state = status['JobRun']['JobRunState']
            while state not in ['SUCCEEDED']:
                time.sleep(10)
                status = client.get_job_run(JobName=jobname, RunId=response['JobRunId'])
                state = status['JobRun']['JobRunState']
                if state in ['STOPPED', 'FAILED', 'TIMEOUT']:
                    raise Exception('Failed to execute glue job: ' + status['JobRun']['ErrorMessage'] + '. State is : ' + state)
        return
    except:
        e = sfdc_exception()
        return e
def process (invoked_job_name,bucket,workflow_control_file_path):
    try:
        read_control_file (invoked_job_name,bucket,workflow_control_file_path)
        pass_arguements_jobs()
        return
    except:
        e = sfdc_exception()
        return e
def main():
    
    arg_list = ['bucket','workflow_control_file_path']
    _job_name = sys.argv[0]
    _job_name = re.split("/",_job_name)[-1].replace(".py","")
    missed_args = []
    for value in arg_list:
        if "--"+value not in sys.argv:
            missed_args.append("--"+value)
    if len(missed_args) > 0:
        format_error_msg = "Improper arguements!!!. Missed arguements are '{}' from the expected list {}".format(missed_args,arg_list)
        print (format_error_msg)
        raise SFDC_GLUE_JOB_ERROR (format_error_msg)
        return
            
    args = getResolvedOptions(sys.argv, arg_list)
    invoked_job_name = _job_name
    bucket = args ['bucket']
    workflow_control_file_path = args ['workflow_control_file_path']
    comments_notes = """
    IMPORTANT NOTES
    ========================================================================================================================================
    iConnect External Territory Program - Workflow can be performed based on requirements with multiple options. 
    The various options illustrates as given below.
    
    Option 1 : ALWAYS FULL LOAD
    ----------------------------------------------------------------------------------------------------------------------------------------
    *Change value 'control_file_path' with file_name '../control_full_load.json' in iconnect_control_workflow.json.
    *This option does not need to load Account object for any comparison to find updated records. Account object is not in part of ingestion.
    
    Option 2 : ALWAYS DELTA LOAD
    ----------------------------------------------------------------------------------------------------------------------------------------
    *Change value 'control_file_path' with file_name '../control_delta_load.json' in iconnect_control_workflow.json.
    *This option find changed records by comparing with Account object and Accoubt object is part of data ingestion.
    
    Option 3 : DELTA LOAD WITHOUT COMPARING ACCOUNT OBJECT
    ----------------------------------------------------------------------------------------------------------------------------------------
    *Change value 'control_file_path' with file_name '../control_delta_previous_load.json' in iconnect_control_workflow.json.
    *This option does not need to load Account object for any comparison to find updated records. Account object is not in part of ingestion.
    *This option find changed records by comparing with previous successful load.
    *Applicable based on your demand in future and it is optional mode.
    
    The optimum approach would be 
    
    Option 1 for first time load. 
    Option 2 for subsequent load.
    ========================================================================================================================================
    """
    print (comments_notes)
    print ("INFO: '{}' - Glue Job started".format(invoked_job_name))
    process (invoked_job_name,bucket,workflow_control_file_path)
    print ("INFO: '{}' - Glue Job completed".format(invoked_job_name))
    return

if __name__ == "__main__":
    main()