CREATE EXTERNAL TABLE IF NOT EXISTS `account_terr_update_full`(
  `id` string, 
  `territory_vod__c` string, 
  `mck_primary_country__c` string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY ',' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://iconnectuserbucket/data/Account_Terr_Update/full/current/data'