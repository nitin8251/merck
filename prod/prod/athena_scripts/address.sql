CREATE EXTERNAL TABLE IF NOT EXISTS `address`(
  `brick_vod__c` string, 
  `id` string, 
  `account_vod__c` string, 
  `country_vod__c` string, 
  `include_in_territory_assignment_vod__c` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://iconnectuserbucket/data/Address_vod__c/current/data'