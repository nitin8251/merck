CREATE EXTERNAL TABLE IF NOT EXISTS `tmp_account_terr_update_delta`(
  `id` string, 
  `territory_vod__c` string, 
  `mck_primary_country__c` string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY ',' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://iconnectuserbucket/data/tmp/data/Account_Terr_Update/delta'