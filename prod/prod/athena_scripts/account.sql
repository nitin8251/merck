CREATE EXTERNAL TABLE IF NOT EXISTS `account`(
  `id` string, 
  `territory_vod__c` string, 
  `mck_primary_country__c` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://iconnectuserbucket/data/Account/current/data'