CREATE EXTERNAL TABLE IF NOT EXISTS `brick_to_terr`(
  `name` string, 
  `mck_brick_to_terr_country__c` string, 
  `territory_vod__c` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://iconnectuserbucket/data/Brick_to_Terr_vod__c/current/data'